/*
 * SPDX-FileCopyrightText: 2021 Cae Lundin <tecc@tecc.me>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-KDE-Accepted-LGPL
 */

export const Data = {
    Name: "name",
    IsRootElement: "root",
    Id: "id",
    ScrollTo: "scrollTo",
    Preset: "preset",
    Page: "page",
    Contents: "contents"
};
export default Data;
