# Request For Comments

**Request For Comments** (RFCs) is a simple project by me (tecc) to make a simple reusable UI for some documents, such as a detailed explanation of a feature request.

This project is in development.

## Branches

| Branch name          | Description                                |
|----------------------|--------------------------------------------|
| `master`             | Main branch                                |
| `tecc/nahw`          | tecc's Not At Home Work (NAHW)             |

## Licence

RFCs is licensed under `LGPL-2.1-or-later`, or `LicenseRef-KDE-Accepted-LGPL`. See the [licensing policy](https://community.kde.org/Policies/Licensing_Policy) for more information.
